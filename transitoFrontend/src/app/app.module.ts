import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { PropietarioComponent } from './propietario/propietario.component';
import { MultaComponent } from './multa/multa.component';
import { VehiculoComponent } from './vehiculo/vehiculo.component';
import { SoatComponent } from './soat/soat.component';
import { TecnomecanicaComponent } from './tecnomecanica/tecnomecanica.component';


@NgModule({
  declarations: [
    AppComponent,
    PropietarioComponent,
    MultaComponent,
    VehiculoComponent,
    SoatComponent,
    TecnomecanicaComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
