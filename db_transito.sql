-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.28-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para db_transito
CREATE DATABASE IF NOT EXISTS `db_transito` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `db_transito`;


-- Volcando estructura para tabla db_transito.multa
CREATE TABLE IF NOT EXISTS `multa` (
  `id_multa` int(10) NOT NULL AUTO_INCREMENT,
  `fecha_multa` date NOT NULL,
  `id_placa` int(7) NOT NULL,
  `descripcion_multa` varchar(30) NOT NULL,
  `estado_multa` tinyint(1) NOT NULL,
  `valor_multa` int(8) NOT NULL,
  `cedula` int(10) NOT NULL,
  PRIMARY KEY (`id_multa`),
  UNIQUE KEY `id_placa` (`id_placa`),
  KEY `cedula` (`cedula`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_transito.multa: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `multa` DISABLE KEYS */;
/*!40000 ALTER TABLE `multa` ENABLE KEYS */;


-- Volcando estructura para tabla db_transito.propietario
CREATE TABLE IF NOT EXISTS `propietario` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `cedula` int(10) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `apellido` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_transito.propietario: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `propietario` DISABLE KEYS */;
INSERT INTO `propietario` (`id`, `cedula`, `nombre`, `apellido`) VALUES
	(2, 0, '', '');
/*!40000 ALTER TABLE `propietario` ENABLE KEYS */;


-- Volcando estructura para tabla db_transito.soat
CREATE TABLE IF NOT EXISTS `soat` (
  `id_soat` int(11) NOT NULL AUTO_INCREMENT,
  `id_placa` int(11) NOT NULL,
  `fecha_expedicion` date NOT NULL,
  `fecha_ini_vigencia` date NOT NULL,
  `fecha_fin_vigencia` date NOT NULL,
  `entidad_soat` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_soat`),
  KEY `id_placa` (`id_placa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_transito.soat: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `soat` DISABLE KEYS */;
/*!40000 ALTER TABLE `soat` ENABLE KEYS */;


-- Volcando estructura para tabla db_transito.tecnomecanica
CREATE TABLE IF NOT EXISTS `tecnomecanica` (
  `id_tecno` int(11) NOT NULL AUTO_INCREMENT,
  `id_placa` int(11) NOT NULL,
  `fecha_ini_vigencia` date NOT NULL,
  `fecha_fin_vigencia` date NOT NULL,
  `entidad_tecno` varchar(50) NOT NULL,
  `fecha_expedicion` date NOT NULL,
  `estado` varchar(10) NOT NULL,
  PRIMARY KEY (`id_tecno`),
  KEY `id_placa` (`id_placa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_transito.tecnomecanica: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tecnomecanica` DISABLE KEYS */;
/*!40000 ALTER TABLE `tecnomecanica` ENABLE KEYS */;


-- Volcando estructura para tabla db_transito.vehiculo
CREATE TABLE IF NOT EXISTS `vehiculo` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `placa` varchar(7) NOT NULL,
  `modelo` varchar(60) NOT NULL,
  `anio` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla db_transito.vehiculo: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `vehiculo` DISABLE KEYS */;
INSERT INTO `vehiculo` (`id`, `placa`, `modelo`, `anio`) VALUES
	(1, 'xsd456', 'Audi R8', 2018);
/*!40000 ALTER TABLE `vehiculo` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
